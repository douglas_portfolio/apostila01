#FOLHA 3
#CLASSE Tabuleiro

CLASSE TabuleiroMissao02 {

    CONSTANTE POSICOES = 22;
    CONSTANTE INICIAL = 0;

    /* Vamos considerar a seguinte estrutura para este Tabuleiro
    
    
    5 - 6 - 7 - 8 - 9 - 10 - 11 - 12 - 13 - 14 (QUIRON)    
    |              (j2)                         |
    4                |                          |
    |              (j1)           15 ---------- |
    3                |             |
    |                |            16
    2 (j3) - j2 - j1 (jav) - 17 -  | - 18 (MONTES GELADOS)
    |                |                  |
    1               22 - 21 - 20 - 19 - |
    |
    0 (inicio)

    */

    VARIAVEL posicaoHeroi : [
        VERDADEIRO,
        FALSO,
        FALSO,
        FALSO,
        FALSO,
        FALSO,
        FALSO,
        FALSO,
        FALSO,
        FALSO,
        FALSO,
        FALSO,
        FALSO,
        FALSO,
        FALSO,
        FALSO,
        FALSO,
        FALSO,
        FALSO,
        FALSO,
        FALSO,

    ]

    METODO:
        
        void atualizarPosicaoHeroi(INTEIRO novaPosicao){
            VARIAVEL tamanhoTabuleiro = POSICOES
            VARIAVEL indice = 0
            ENQUANTO (indice < tamanhoTabuleiro && indice != novaPosicao){
                posicaoHeroi[indice] = FALSO
                indice = indice + 1
            }
            posicaoHeroi[novaPosicao] = VERDADIRO
        }

        INTEIRO jogarDado(INTEIRO qtdDados){
            //RETORNA UM VALOR ENTRE 0 E 6 * qtdDados
            //Para jogar o aluno deverá jogar os dados de verdade
        }

}