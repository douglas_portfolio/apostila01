#Folha 4
#CLASSE Javali #4

CLASSE Javali {

    VARIAVEL posicaoAtual : INTEIRO
    VARIAVEL vidasMonstro : INTEIRO
    
    METODO:
        BOOLEANO atacar(Heroi hercules, INTEIRO qtdCasasParaMatar){
            INTEIRO dado = jogaDado(1)
            BOOLEANO jogoContinua = VERDADEIRO
            SE(dado < qtdCasasParaMatar){
                ESCREVA('Hercules salvo')
            }
            SENAO{
                SE(hercules.estaDeEscudo()){
                    ESCREVA('Sorte sua! Está de escudo!')
                }
                SENAO{
                    hercules.vidaHeroi = 0
                    jogoContinua = FALSO
                }
            }
            RETORNA jogoContinua
        }
}
