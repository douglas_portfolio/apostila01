#MAIN
#IMPORT
    #Heroi #1
    #Corca #4

MAIN:
    VARIAVEL hercules : Heroi;
    CONSTANTE VIDAS_HERCULES = 20
    CONSTANTE VIDAS_CORCA = 10
    CONSTANTE POSICAO_CORCA = 5
    CONSTANTE POSICAO_HERCULES = 0

    VARIAVEL hercules = NEW Heroi()
    hercules.vidaHeroi = VIDAS_HERCULES
    hercules.posicaoAtual = POSICAO_HERCULES

    VARIAVEL corca = NEW Corca()
    corca.vidasMonstro = VIDAS_CORCA
    corca.posicaoAtual = POSICAO_CORCA

    VARIAVEL quironJunto = FALSO



PROGRAMA:

    METODO:
       VOID percorrerTabuleiro(){
           escolherDirecao();
           INTEIRO dado = jogaDado(1)
           BOOLEANO parar = FALSO

           ENQUANTO(parar == FALSO){
                SE(hercules.posicaoAtual <= 14){
                    escolherDirecao()
                    hercules.posicaoAtual = hercules.posicaoAtual + dado 
                    SE(hercules.posicaoAtual >  14){
                        hercules.posicaoAtual = 14
                        quironJunto = TRUE
                        tabuleiro.atualizarPosicaoHeroi(hercules.posicaoAtual)
                    }
                }
                SE(quironJunto){
                    escolherDirecao()
                    dado = jogaDado(1)
                    hercules.posicaoAtual = hercules.posicaoAtual + dado
                    SE( hercules.posicaoAtual > tabuleiro.POSICOES ){
                        hercules.posicaoAtual = hercules.posicaoAtual - jogaDado(1)
                        SE(hercules.posicaoAtual < 14){
                            quironJunto = FALSO
                        }
                    }
                    SE(hercules.posicaoAtual == 18){
                        parar = VERDADEIRO
                        ESCREVER('VENCEU O JOGO')
                    }
                    SE(hercules.posicaoAtual == 17 || hercules.posicaoAtual == 22){
                        parar = !javali.atacar(hercules, 2)
                        SE(parar){
                            ESCREVER('GAME OVER')
                        }
                    }
                    
                }

                SE(hercules.posicaoAtual == 2){
                    parar = !javali.atacar(hercules, 3)
                    SE(parar){
                        ESCREVER('GAME OVER')
                    }
                }
                SE(hercules.posicaoAtual == 9){
                    parar = !javali.atacar(hercules, 2)
                    SE(parar){
                        ESCREVER('GAME OVER')
                    }
                }
            }
       }
       

    
