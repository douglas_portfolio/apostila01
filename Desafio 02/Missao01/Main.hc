#MAIN
#IMPORT
    #Heroi #1
    #Leao #4

MAIN:
    VARIAVEL hercules : Heroi;
    CONSTANTE VIDAS_HERCULES = 20
    CONSTANTE VIDAS_HIDRA = 10 * 6

    VARIAVEL hercules = NEW Heroi()
    hercules.vidaHeroi = VIDAS_HERCULES

    VARIAVEL hidra = NEW Hidra()
    hidra.vidasMonstro = VIDAS_HIDRA



PROGRAMA:

    METODO:
        /* Escolheremos que o Hercules sempre irá bater primeiro.
        Você pode ter escolhido diferente */
        VOID lutar(Leao leao, Heroi hercules){
            BOOLEANO parar = FALSO
            ENQUANTO (parar == FALSO && !verificaMorte(leao, hercules)){
                VARIAVEL dadosLeao = leao.jogarDados()
                VARIAVEL dadosHercules = hercules.jogarDados()
                leao.apanhar(dadosHercules)
                SE(leao.estaMorto()){
                    ESCREVA('Hercules')
                    parar = VERDADEIRO
                }
                SENAO{
                    hercules.apanhar(dadosLeao)
                    SE(hercules.estaMorto()){
                        ESCREVA('Leão')
                        parar = VERDADEIRO
                        hercules.avancarDesafio()
                    }
                }
            }
        }

        BOOLEANO verificaMorte(Leao leao, Heroi hercules){
            RETORNA leao.estaMorto() || hercules.estaMorto
        }

    
