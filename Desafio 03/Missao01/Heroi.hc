#Folha 01
#CLASSE HEROI
#IMPORT
    # TabuleiroMissao02 #3
    # Direcao #2


CLASSE Heroi {
    VARIAVEL irritabilidade : REAL
    VARIAVEL armas : BOOLEANO
    VARIAVEL posicaoAtual : INTEIRO;
    VARIAVEL vidaHeroi : INTEIRO;

    CONSTANTE tabueleiro : TabuleiroMissao02;
    tabueleiro = NEW TabuleiroMissao02();

    METODO 
        VOID receberAjuda(){
            armas = VERDADEIRO;
        }

        VOID andar(DIRECAO direcao, INTEIRO quantidadeDePassos){
            SE(direcao == DIRECAO.CIMA || direcao == DIRECAO.DIREITA){
                posicaoAtual = posicaoAtual + quantidadeDePassos;
            }
        }

        VOID atualizarVida(INTEIRO vida){
            vidaHeroi = vida;
        }

        VOID apanhar(INTEIRO valor){
            vidaHeroi = vidaHeroi - valor
        }

        BOOLEANO estaMorto(){
            RETORNA vidaHeroi <= 0
        }

        VOID avancarDesafio(){
            //Não foi dito como avançar o desafio. Logo, não precisa implementar.
        }

        VOID correr(){
            INTEIRO somaDados = jogaDado(2)
            RETORNA 2 * somaDados
        }

}