#MAIN
#IMPORT
    #Heroi #1
    #Corca #4

MAIN:
    VARIAVEL hercules : Heroi;
    CONSTANTE VIDAS_HERCULES = 20
    CONSTANTE VIDAS_CORCA = 10
    CONSTANTE POSICAO_CORCA = 5
    CONSTANTE POSICAO_HERCULES = 0

    VARIAVEL hercules = NEW Heroi()
    hercules.vidaHeroi = VIDAS_HERCULES
    hercules.posicaoAtual = POSICAO_HERCULES

    VARIAVEL corca = NEW Corca()
    corca.vidasMonstro = VIDAS_CORCA
    corca.posicaoAtual = POSICAO_CORCA



PROGRAMA:

    METODO:
        BOOLEANO capturarCorca(){
            INTEIRO tentativa = 10
            BOOLEANO alcancou = FALSO
            ENQUANTO (tentativa > 0 || !alcancou){
                hercules.correr()
                corca.correr()
                SE(hercules.posicaoAtual > corca.posicaoAtual){
                    alcancou = VERDADEIRO
                }
                tentativa = tentativa - 1
            }
            RETORNA alcancou
        }
       

    
