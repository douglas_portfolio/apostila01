#FOLHA 4
#CLASSE LEAO
#IMPORT
    # Heroi #1
    # TabuleiroMissao02 #3

// Assumiremos aqui a existência de um método chamado
// jogarDados(INTEIRO N) que receberá como parâmetro
// o número de dados. Este método é parte da CLASSE
// tabueleiro.

CLASSE Leao {

    CONSTANTE tabueleiro : TabuleiroMissao02;
    tabueleiro = NEW TabuleiroMissao02();

    VARIAVEL vidaLeao : INTEIRO
    
    METODO:
        INTEIRO jogarDados(){
            RETORNA tabuleiro.jogarDados(2);
        }

        VOID atualizarVida(INTEIRO vida){
            vidaLeao = vida;
        }

        VOID apanhar(INTEIRO valor){
            vidaLeao = vidaLeao - valor
        }

        BOOLEANO estaMorto(){
            RETORNA vidaLeao <= 0
        }


}