#MAIN
#IMPORT
    #Heroi #1
    #Leao #4

MAIN:
    VARIAVEL hercules : Heroi;
    CONSTANTE VIDAS_HERCULES = 20

    hercules = NEW Heroi()
    hercules.vida = VIDAS_HERCULES

    leao = NEW Leao()


PROGRAMA:

    METODO:
        /* Escolheremos que o Hercules sempre irá bater primeiro.
        Você pode ter escolhido diferente */
        VOID lutar(Leao leao, Heroi hercules){
            VARIAVEL dadosLeao = leao.jogarDados()
            VARIAVEL dadosHercules = hercules.jogarDados()
            leao.apanhar(dadosHercules)
            SE(leao.estaMorto()){
                ESCREVA('Hércules')
            }
            SENAO{
                hercules.apanhar(dadosLeao)
                SE(hercules.estaMorto()){
                    ESCREVA('Leão')
                }
            }
            
        }

    
