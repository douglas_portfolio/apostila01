#Folha 01
#CLASSE HEROI
#IMPORT
    # TabuleiroMissao02 #3
    # Direcao #2


CLASSE Heroi {
    VARIAVEL irritabilidade : REAL
    VARIAVEL armas : BOOLEANO
    VARIAVEL posicaoAtual = 0
    VARIAVEL vidaHeroi : INTEIRO;

    CONSTANTE tabueleiro : TabuleiroMissao02;
    tabueleiro = NEW TabuleiroMissao02();

    METODO VOID receberAjuda(){
        armas = VERDADEIRO;
    }

    METODO VOID andar(DIRECAO direcao, INTEIRO quantidadeDePassos){
        if(direcao == DIRECAO.CIMA || direcao == DIRECAO.DIREITA){
            posicaoAtual = posicaoAtual + quantidadeDePassos;
        }
    }

    METODO VOID atualizarVida(INTEIRO vida){
        vidaHeroi = vida;
    }

    METODO VOID apanhar(INTEIRO valor){
        vidaHeroi = vidaHeroi - valor
    }

    METODO BOOLEANO estaMorto(){
        RETORNA vidaHeroi <= 0
    }

}