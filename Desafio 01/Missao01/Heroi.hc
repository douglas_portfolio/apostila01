#Folha 01
#CLASSE HEROI

#IMPORT
    # TabuleiroMissao01 #3
    # Direcao #2

CLASSE Heroi {
    VARIAVEL irritabilidade : REAL
    VARIAVEL armas : BOOLEANO
    VARIAVEL posicaoAtual = 0
    VARIAVEL vida : INTEIRO;

    CONSTANTE tabueleiro : TabuleiroMissao01;
    tabueleiro = NEW TabuleiroMissao01();

    METODO receberAjuda(){
        armas = VERDADEIRO;
    }

    METODO:
        VOID andar(DIRECAO direcao, INTEIRO quantidadeDePassos){
            SE(direcao == DIRECAO.CIMA || direcao == DIRECAO.DIREITA){
                posicaoAtual = posicaoAtual + quantidadeDePassos;
            }
            SENAO{
                posicaoAtual = posicaoAtual + quantidadeDePassos;
            }
            tabuleiro.atualizaPosicaoHeroi(posicaoAtual)
        }

}