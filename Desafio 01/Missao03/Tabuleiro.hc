#FOLHA 3
#CLASSE Tabuleiro

CLASSE TabuleiroMissao02 {

    CONSTANTE POSICOES = 4;
    CONSTANTE INICIAL = 0;
    CONSTANTE NEMEIA = 4;

    VARIAVEL posicaoHeroi : [
        VERDADEIRO,
        FALSO,
        FALSO,
        FALSO,
        FALSO
    ]

    METODO:
        BOOLEANO chegouANemeia(INTEIRO posicaoAtual){
            RETORNA posicaoAtual == MEMEIA
        }

        VOID atualizaPosicaoHeroi(INTEIRO posicaoAtual){
            SE(posicaoAtual >= NEMEIA){
                posicaoAtual = NEMEIA;
            }
            SE(posicaoAtual < INICIAL){
                posicaoAtual = INICIAL;
            }
            posicaoHeroi[posicaoAtual] = VERDADIRO;
            INTEIRO i = 0;
            ENQUANTO (i < NEMEIA && i != posicaoAtual) {
                posicaoHeroi[i] = FALSO;
            }
        }

        INTEIRO jogarDado(INTEIRO qtdDados){
            //RETORNA UM VALOR ENTRE 0 E 6 * qtdDados
            //Para jogar o aluno deverá jogar os dados de verdade
        }

}