#MAIN
#IMPORT
    #Heroi #1
    #Leao #4

MAIN:
    VARIAVEL hercules : Heroi;
    CONSTANTE VIDAS_HERCULES = 20
    CONSTANTE VIDAS_LEAO = 30

    hercules = NEW Heroi()
    hercules.vida = VIDAS_HERCULES

    leao = NEW Leao()


PROGRAMA:

    METODO:
        /* Escolheremos que o Hercules sempre irá bater primeiro.
        Você pode ter escolhido diferente */
        VOID lutar(Leao leao, Heroi hercules){
            BOOLEANO parar = FALSO
            ENQUANTO (parar == FALSO && !verificaMorte(leao, hercules)){
                VARIAVEL dadosLeao = leao.jogarDados()
                VARIAVEL dadosHercules = hercules.jogarDados()
                leao.apanhar(dadosHercules)
                SE(leao.estaMorto()){
                    ESCREVA('Hercules')
                    parar = VERDADEIRO
                }
                SENAO{
                    hercules.apanhar(dadosLeao)
                    SE(hercules.estaMorto()){
                        ESCREVA('Leão')
                        parar = VERDADEIRO
                        hercules.avancarDesafio()
                    }
                }
            }
        }

        BOOLEANO verificaMorte(Leao leao, Heroi hercules){
            RETORNA leao.estaMorto() || hercules.estaMorto
        }

    
